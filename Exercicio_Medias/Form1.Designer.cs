﻿namespace Exercicio_Medias
{
    partial class frmMedias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.lblB4 = new System.Windows.Forms.Label();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.lblB3 = new System.Windows.Forms.Label();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.lblB2 = new System.Windows.Forms.Label();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.txtAluno = new System.Windows.Forms.TextBox();
            this.lblAluno = new System.Windows.Forms.Label();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.grpAlunos = new System.Windows.Forms.GroupBox();
            this.grdAlunos = new System.Windows.Forms.DataGridView();
            this.grpSituacao = new System.Windows.Forms.GroupBox();
            this.txtSituacao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMedia = new System.Windows.Forms.TextBox();
            this.lblMedia = new System.Windows.Forms.Label();
            this.grpEstatisticas = new System.Windows.Forms.GroupBox();
            this.txtMediaTurma = new System.Windows.Forms.TextBox();
            this.lblMediaTurma = new System.Windows.Forms.Label();
            this.txtRecuperacao = new System.Windows.Forms.TextBox();
            this.lblRecuperacao = new System.Windows.Forms.Label();
            this.txtReprovados = new System.Windows.Forms.TextBox();
            this.lblReprovados = new System.Windows.Forms.Label();
            this.txtAprovados = new System.Windows.Forms.TextBox();
            this.lblAprovados = new System.Windows.Forms.Label();
            this.txtMenor = new System.Windows.Forms.TextBox();
            this.lblMenor = new System.Windows.Forms.Label();
            this.txtMaior = new System.Windows.Forms.TextBox();
            this.lblMaior = new System.Windows.Forms.Label();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.aluno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdB1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdB2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdB3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdB4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpDados.SuspendLayout();
            this.grpAlunos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAlunos)).BeginInit();
            this.grpSituacao.SuspendLayout();
            this.grpEstatisticas.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtB4);
            this.grpDados.Controls.Add(this.lblB4);
            this.grpDados.Controls.Add(this.txtB3);
            this.grpDados.Controls.Add(this.lblB3);
            this.grpDados.Controls.Add(this.txtB2);
            this.grpDados.Controls.Add(this.lblB2);
            this.grpDados.Controls.Add(this.txtB1);
            this.grpDados.Controls.Add(this.lblB1);
            this.grpDados.Controls.Add(this.txtAluno);
            this.grpDados.Controls.Add(this.lblAluno);
            this.grpDados.Location = new System.Drawing.Point(12, 12);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(404, 108);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados";
            // 
            // txtB4
            // 
            this.txtB4.Location = new System.Drawing.Point(248, 68);
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(64, 20);
            this.txtB4.TabIndex = 9;
            this.txtB4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB4
            // 
            this.lblB4.Location = new System.Drawing.Point(168, 73);
            this.lblB4.Name = "lblB4";
            this.lblB4.Size = new System.Drawing.Size(100, 15);
            this.lblB4.TabIndex = 8;
            this.lblB4.Text = "4. Bimestre";
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(96, 68);
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(64, 20);
            this.txtB3.TabIndex = 7;
            this.txtB3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB3
            // 
            this.lblB3.Location = new System.Drawing.Point(16, 73);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(100, 15);
            this.lblB3.TabIndex = 6;
            this.lblB3.Text = "3. Bimestre";
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(248, 40);
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(64, 20);
            this.txtB2.TabIndex = 5;
            this.txtB2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB2
            // 
            this.lblB2.Location = new System.Drawing.Point(168, 45);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(100, 15);
            this.lblB2.TabIndex = 4;
            this.lblB2.Text = "2. Bimestre";
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(96, 40);
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(64, 20);
            this.txtB1.TabIndex = 3;
            this.txtB1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB1
            // 
            this.lblB1.Location = new System.Drawing.Point(16, 45);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(100, 15);
            this.lblB1.TabIndex = 2;
            this.lblB1.Text = "1. Bimestre";
            // 
            // txtAluno
            // 
            this.txtAluno.Location = new System.Drawing.Point(96, 16);
            this.txtAluno.Name = "txtAluno";
            this.txtAluno.Size = new System.Drawing.Size(296, 20);
            this.txtAluno.TabIndex = 1;
            // 
            // lblAluno
            // 
            this.lblAluno.Location = new System.Drawing.Point(16, 21);
            this.lblAluno.Name = "lblAluno";
            this.lblAluno.Size = new System.Drawing.Size(100, 15);
            this.lblAluno.TabIndex = 0;
            this.lblAluno.Text = "Aluno";
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Location = new System.Drawing.Point(428, 20);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(128, 28);
            this.btnAdicionar.TabIndex = 1;
            this.btnAdicionar.Text = "&Adicionar aluno";
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(428, 56);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(128, 28);
            this.btnLimpar.TabIndex = 2;
            this.btnLimpar.Text = "&Limpar campos";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(428, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 28);
            this.button1.TabIndex = 3;
            this.button1.Text = "&Calcular estatísticas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grpAlunos
            // 
            this.grpAlunos.Controls.Add(this.grdAlunos);
            this.grpAlunos.Location = new System.Drawing.Point(12, 132);
            this.grpAlunos.Name = "grpAlunos";
            this.grpAlunos.Size = new System.Drawing.Size(544, 184);
            this.grpAlunos.TabIndex = 4;
            this.grpAlunos.TabStop = false;
            this.grpAlunos.Text = "Alunos cadastrados";
            // 
            // grdAlunos
            // 
            this.grdAlunos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAlunos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.aluno,
            this.grdB1,
            this.grdB2,
            this.grdB3,
            this.grdB4});
            this.grdAlunos.Location = new System.Drawing.Point(8, 20);
            this.grdAlunos.MultiSelect = false;
            this.grdAlunos.Name = "grdAlunos";
            this.grdAlunos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdAlunos.Size = new System.Drawing.Size(520, 150);
            this.grdAlunos.TabIndex = 0;
            this.grdAlunos.TabStop = false;
            this.grdAlunos.DoubleClick += new System.EventHandler(this.grdAlunos_DoubleClick);
            // 
            // grpSituacao
            // 
            this.grpSituacao.Controls.Add(this.txtSituacao);
            this.grpSituacao.Controls.Add(this.label1);
            this.grpSituacao.Controls.Add(this.txtMedia);
            this.grpSituacao.Controls.Add(this.lblMedia);
            this.grpSituacao.Location = new System.Drawing.Point(12, 324);
            this.grpSituacao.Name = "grpSituacao";
            this.grpSituacao.Size = new System.Drawing.Size(200, 92);
            this.grpSituacao.TabIndex = 5;
            this.grpSituacao.TabStop = false;
            this.grpSituacao.Text = "Situação do aluno";
            // 
            // txtSituacao
            // 
            this.txtSituacao.Location = new System.Drawing.Point(68, 45);
            this.txtSituacao.Name = "txtSituacao";
            this.txtSituacao.ReadOnly = true;
            this.txtSituacao.Size = new System.Drawing.Size(116, 20);
            this.txtSituacao.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Situação";
            // 
            // txtMedia
            // 
            this.txtMedia.Location = new System.Drawing.Point(68, 20);
            this.txtMedia.Name = "txtMedia";
            this.txtMedia.ReadOnly = true;
            this.txtMedia.Size = new System.Drawing.Size(64, 20);
            this.txtMedia.TabIndex = 5;
            this.txtMedia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMedia
            // 
            this.lblMedia.Location = new System.Drawing.Point(8, 23);
            this.lblMedia.Name = "lblMedia";
            this.lblMedia.Size = new System.Drawing.Size(100, 15);
            this.lblMedia.TabIndex = 4;
            this.lblMedia.Text = "Média";
            // 
            // grpEstatisticas
            // 
            this.grpEstatisticas.Controls.Add(this.txtMediaTurma);
            this.grpEstatisticas.Controls.Add(this.lblMediaTurma);
            this.grpEstatisticas.Controls.Add(this.txtRecuperacao);
            this.grpEstatisticas.Controls.Add(this.lblRecuperacao);
            this.grpEstatisticas.Controls.Add(this.txtReprovados);
            this.grpEstatisticas.Controls.Add(this.lblReprovados);
            this.grpEstatisticas.Controls.Add(this.txtAprovados);
            this.grpEstatisticas.Controls.Add(this.lblAprovados);
            this.grpEstatisticas.Controls.Add(this.txtMenor);
            this.grpEstatisticas.Controls.Add(this.lblMenor);
            this.grpEstatisticas.Controls.Add(this.txtMaior);
            this.grpEstatisticas.Controls.Add(this.lblMaior);
            this.grpEstatisticas.Location = new System.Drawing.Point(232, 324);
            this.grpEstatisticas.Name = "grpEstatisticas";
            this.grpEstatisticas.Size = new System.Drawing.Size(324, 148);
            this.grpEstatisticas.TabIndex = 6;
            this.grpEstatisticas.TabStop = false;
            this.grpEstatisticas.Text = "Estatísticas";
            // 
            // txtMediaTurma
            // 
            this.txtMediaTurma.Location = new System.Drawing.Point(104, 116);
            this.txtMediaTurma.Name = "txtMediaTurma";
            this.txtMediaTurma.ReadOnly = true;
            this.txtMediaTurma.Size = new System.Drawing.Size(68, 20);
            this.txtMediaTurma.TabIndex = 17;
            // 
            // lblMediaTurma
            // 
            this.lblMediaTurma.Location = new System.Drawing.Point(8, 119);
            this.lblMediaTurma.Name = "lblMediaTurma";
            this.lblMediaTurma.Size = new System.Drawing.Size(100, 15);
            this.lblMediaTurma.TabIndex = 16;
            this.lblMediaTurma.Text = "Média da turma";
            // 
            // txtRecuperacao
            // 
            this.txtRecuperacao.Location = new System.Drawing.Point(104, 92);
            this.txtRecuperacao.Name = "txtRecuperacao";
            this.txtRecuperacao.ReadOnly = true;
            this.txtRecuperacao.Size = new System.Drawing.Size(196, 20);
            this.txtRecuperacao.TabIndex = 15;
            // 
            // lblRecuperacao
            // 
            this.lblRecuperacao.Location = new System.Drawing.Point(8, 95);
            this.lblRecuperacao.Name = "lblRecuperacao";
            this.lblRecuperacao.Size = new System.Drawing.Size(100, 15);
            this.lblRecuperacao.TabIndex = 14;
            this.lblRecuperacao.Text = "Aunos recuperação";
            // 
            // txtReprovados
            // 
            this.txtReprovados.Location = new System.Drawing.Point(104, 68);
            this.txtReprovados.Name = "txtReprovados";
            this.txtReprovados.ReadOnly = true;
            this.txtReprovados.Size = new System.Drawing.Size(196, 20);
            this.txtReprovados.TabIndex = 13;
            // 
            // lblReprovados
            // 
            this.lblReprovados.Location = new System.Drawing.Point(8, 71);
            this.lblReprovados.Name = "lblReprovados";
            this.lblReprovados.Size = new System.Drawing.Size(100, 15);
            this.lblReprovados.TabIndex = 12;
            this.lblReprovados.Text = "Alunos reprovados";
            // 
            // txtAprovados
            // 
            this.txtAprovados.Location = new System.Drawing.Point(104, 44);
            this.txtAprovados.Name = "txtAprovados";
            this.txtAprovados.ReadOnly = true;
            this.txtAprovados.Size = new System.Drawing.Size(196, 20);
            this.txtAprovados.TabIndex = 11;
            // 
            // lblAprovados
            // 
            this.lblAprovados.Location = new System.Drawing.Point(8, 47);
            this.lblAprovados.Name = "lblAprovados";
            this.lblAprovados.Size = new System.Drawing.Size(100, 15);
            this.lblAprovados.TabIndex = 10;
            this.lblAprovados.Text = "Alunos aprovados";
            // 
            // txtMenor
            // 
            this.txtMenor.Location = new System.Drawing.Point(236, 16);
            this.txtMenor.Name = "txtMenor";
            this.txtMenor.ReadOnly = true;
            this.txtMenor.Size = new System.Drawing.Size(64, 20);
            this.txtMenor.TabIndex = 9;
            this.txtMenor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMenor
            // 
            this.lblMenor.Location = new System.Drawing.Point(169, 19);
            this.lblMenor.Name = "lblMenor";
            this.lblMenor.Size = new System.Drawing.Size(100, 15);
            this.lblMenor.TabIndex = 8;
            this.lblMenor.Text = "Menor média";
            // 
            // txtMaior
            // 
            this.txtMaior.Location = new System.Drawing.Point(104, 16);
            this.txtMaior.Name = "txtMaior";
            this.txtMaior.ReadOnly = true;
            this.txtMaior.Size = new System.Drawing.Size(64, 20);
            this.txtMaior.TabIndex = 7;
            this.txtMaior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMaior
            // 
            this.lblMaior.Location = new System.Drawing.Point(8, 19);
            this.lblMaior.Name = "lblMaior";
            this.lblMaior.Size = new System.Drawing.Size(100, 15);
            this.lblMaior.TabIndex = 6;
            this.lblMaior.Text = "Maior média";
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(48, 436);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(128, 28);
            this.btnIniciar.TabIndex = 7;
            this.btnIniciar.Text = "&Iniciar nova turma";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // aluno
            // 
            this.aluno.Frozen = true;
            this.aluno.HeaderText = "Aluno";
            this.aluno.Name = "aluno";
            this.aluno.ReadOnly = true;
            this.aluno.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.aluno.Width = 210;
            // 
            // grdB1
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdB1.DefaultCellStyle = dataGridViewCellStyle5;
            this.grdB1.Frozen = true;
            this.grdB1.HeaderText = "1. Bim";
            this.grdB1.Name = "grdB1";
            this.grdB1.ReadOnly = true;
            this.grdB1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdB1.Width = 60;
            // 
            // grdB2
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdB2.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdB2.Frozen = true;
            this.grdB2.HeaderText = "2. Bim";
            this.grdB2.Name = "grdB2";
            this.grdB2.ReadOnly = true;
            this.grdB2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdB2.Width = 60;
            // 
            // grdB3
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdB3.DefaultCellStyle = dataGridViewCellStyle7;
            this.grdB3.Frozen = true;
            this.grdB3.HeaderText = "3. Bim";
            this.grdB3.Name = "grdB3";
            this.grdB3.ReadOnly = true;
            this.grdB3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdB3.Width = 60;
            // 
            // grdB4
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdB4.DefaultCellStyle = dataGridViewCellStyle8;
            this.grdB4.Frozen = true;
            this.grdB4.HeaderText = "4. Bim";
            this.grdB4.Name = "grdB4";
            this.grdB4.ReadOnly = true;
            this.grdB4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdB4.Width = 60;
            // 
            // frmMedias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 487);
            this.Controls.Add(this.btnIniciar);
            this.Controls.Add(this.grpEstatisticas);
            this.Controls.Add(this.grpSituacao);
            this.Controls.Add(this.grpAlunos);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.grpDados);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMedias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controle de Médias";
            this.Load += new System.EventHandler(this.frmMedias_Load);
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.grpAlunos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAlunos)).EndInit();
            this.grpSituacao.ResumeLayout(false);
            this.grpSituacao.PerformLayout();
            this.grpEstatisticas.ResumeLayout(false);
            this.grpEstatisticas.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.TextBox txtAluno;
        private System.Windows.Forms.Label lblAluno;
        private System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.Label lblB4;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.Label lblB3;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.Label lblB1;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox grpAlunos;
        private System.Windows.Forms.DataGridView grdAlunos;
        private System.Windows.Forms.GroupBox grpSituacao;
        private System.Windows.Forms.TextBox txtSituacao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMedia;
        private System.Windows.Forms.Label lblMedia;
        private System.Windows.Forms.GroupBox grpEstatisticas;
        private System.Windows.Forms.TextBox txtMaior;
        private System.Windows.Forms.Label lblMaior;
        private System.Windows.Forms.TextBox txtMenor;
        private System.Windows.Forms.Label lblMenor;
        private System.Windows.Forms.TextBox txtAprovados;
        private System.Windows.Forms.Label lblAprovados;
        private System.Windows.Forms.TextBox txtRecuperacao;
        private System.Windows.Forms.Label lblRecuperacao;
        private System.Windows.Forms.TextBox txtReprovados;
        private System.Windows.Forms.Label lblReprovados;
        private System.Windows.Forms.TextBox txtMediaTurma;
        private System.Windows.Forms.Label lblMediaTurma;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.DataGridViewTextBoxColumn aluno;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdB1;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdB2;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdB3;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdB4;
    }
}

