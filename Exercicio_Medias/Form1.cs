﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exercicio_Medias
{
    public partial class frmMedias : Form
    {
        public frmMedias()
        {
            InitializeComponent();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            int i;
            bool encontrado;
            float b1, b2, b3, b4;
            if (txtAluno.Text != "" && txtB1.Text != "" && txtB2.Text != "" && txtB3.Text != "" && txtB4.Text != "")
            {
                i = 0;
                encontrado = false;
                while (i < grdAlunos.Rows.Count - 1 && encontrado == false)
                {
                    if (grdAlunos.Rows[i].Cells["aluno"].Value.ToString() == txtAluno.Text)
                    {
                        encontrado = true;
                    }
                    i++;
                }
                if (encontrado == false)
                {
                    try
                    {
                        b1 = float.Parse(txtB1.Text);
                        b2 = float.Parse(txtB2.Text);
                        b3 = float.Parse(txtB3.Text);
                        b4 = float.Parse(txtB4.Text);
                        grdAlunos.Rows.Add(txtAluno.Text, b1.ToString("N2"), b2.ToString("N2"), b3.ToString("N2"), b4.ToString("N2"));
                        btnLimpar_Click(sender, e);
                        txtAluno.Focus();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show("Notas inválidas", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtB1.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Aluno já cadastrado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtAluno.Focus();
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtAluno.Focus();
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtAluno.Clear();
            txtB1.Clear();
            txtB2.Clear();
            txtB3.Clear();
            txtB4.Clear();
        }

        private void grdAlunos_DoubleClick(object sender, EventArgs e)
        {
            float b1, b2, b3, b4, media;
            try
            {
                b1 = float.Parse(grdAlunos.CurrentRow.Cells["grdB1"].Value.ToString());
                b2 = float.Parse(grdAlunos.CurrentRow.Cells["grdB2"].Value.ToString());
                b3 = float.Parse(grdAlunos.CurrentRow.Cells["grdB3"].Value.ToString());
                b4 = float.Parse(grdAlunos.CurrentRow.Cells["grdB4"].Value.ToString());
                media = (b1 + b2 + b3 + b4) / 4;
                txtMedia.Text = media.ToString("N2");
                if (media >= 7)
                {
                    txtSituacao.Text = "APROVADO";                    
                }
                else
                {
                    if (media < 2.5)
                    {
                        txtSituacao.Text = "REPROVADO";
                    }
                    else
                    {
                        txtSituacao.Text = "RECUPERAÇÃO";
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Aluno inválido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float b1, b2, b3, b4, media, maior = 0, menor = 0, pAp, pRep, pRec, total = 0;
            int i, ap = 0, rep = 0, rec = 0;
            for (i = 0; i < grdAlunos.Rows.Count - 1; i++)
            {
                b1 = float.Parse(grdAlunos.Rows[i].Cells["grdB1"].Value.ToString());
                b2 = float.Parse(grdAlunos.Rows[i].Cells["grdB2"].Value.ToString());
                b3 = float.Parse(grdAlunos.Rows[i].Cells["grdB3"].Value.ToString());
                b4 = float.Parse(grdAlunos.Rows[i].Cells["grdB4"].Value.ToString());
                media = (b1 + b2 + b3 + b4) / 4;
                if (i == 0)
                {
                    maior = media;
                    menor = media;
                }
                else
                {
                    if (media > maior)
                    {
                        maior = media;
                    }
                    if (media < menor)
                    {
                        menor = media;
                    }
                }
                if (media >= 7)
                {
                    ap++;
                }
                else
                {
                    if (media < 2.5)
                    {
                        rep++;
                    }
                    else
                    {
                        rec++;
                    }
                }
                total += media;
            }
            txtMaior.Text = maior.ToString("N2");
            txtMenor.Text = menor.ToString("N2");
            pAp = (100 * ap) / (grdAlunos.Rows.Count - 1);
            pRep = (100 * rep) / (grdAlunos.Rows.Count - 1);
            pRec = (100 * rec) / (grdAlunos.Rows.Count - 1);
            txtAprovados.Text = ap.ToString() + " - " + pAp.ToString("N2") + "%";
            txtReprovados.Text = rep.ToString() + " - " + pRep.ToString("N2") + "%";
            txtRecuperacao.Text = rec.ToString() + " - " + pRec.ToString("N2") + "%";
            txtMediaTurma.Text = (total / (grdAlunos.Rows.Count - 1)).ToString("N2");
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            btnLimpar_Click(sender, e);
            grdAlunos.Rows.Clear();
            txtMaior.Clear();
            txtMenor.Clear();
            txtAprovados.Clear();
            txtReprovados.Clear();
            txtRecuperacao.Clear();
            txtMediaTurma.Clear();
            txtMedia.Clear();
            txtSituacao.Clear();
            txtAluno.Focus();
        }

        private void frmMedias_Load(object sender, EventArgs e)
        {

        }


    }
}
